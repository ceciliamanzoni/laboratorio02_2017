package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	
	private int[] rolls = new int[20];
	private int currentRoll = 0;

	@Override
	public void roll(int pins) {
		rolls[currentRoll++]=pins;

	}

	@Override
	public int score() {
		int score=0;
		for(int currentRoll=0;currentRoll<20;currentRoll++){
			if(isSpare(currentRoll))
				score += rolls[currentRoll];
			score += rolls[currentRoll];
		}
		return score;
	}
	
	
	private boolean isSpare(int currentRoll){
		return (currentRoll >1 && rolls[currentRoll-1]+rolls[currentRoll-2]==10);
		
	}

}
